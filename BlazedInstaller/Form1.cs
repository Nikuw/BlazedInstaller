﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Compression;


namespace BlazedInstaller {
	public partial class MainWindow : Form {
		public MainWindow() {
			InitializeComponent();
		}

		private void InstallButton_Click(object sender, EventArgs e) {
			InstallButton.Enabled = false;
			Status.Text = $"Pobieranie pliku {Variables.blazingUrl}";
			DownloadFile(Variables.blazingUrl, Variables.zipPath);
		}


		public void DownloadFile(string urlAddress, string location) {
			using (Variables.webClient = new WebClient()) {
				Variables.webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
				Variables.webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);

				// The variable that will be holding the url address (making sure it starts with http://)
				Uri URL = urlAddress.StartsWith("http://", StringComparison.OrdinalIgnoreCase) ? new Uri(urlAddress) : new Uri("http://" + urlAddress);

				// Start the stopwatch which we will be using to calculate the download speed
				Variables.sw.Start();

				try {
					// Start downloading the file
					Variables.webClient.DownloadFileAsync(URL, location);
				} catch (Exception ex) {
					MessageBox.Show($"To się nie powinno pojawić! Powiadom mnie o tym błędzie: {ex.Message}");
				}
			}
		}

		private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e) {
			// Calculate download speed and output it to labelSpeed.
			labelSpeed.Text = string.Format("{0} kB/s", (e.BytesReceived / 1024d / Variables.sw.Elapsed.TotalSeconds).ToString("0.00"));

			// Update the progressbar percentage only when the value is not the same.
			progressBar.Value = e.ProgressPercentage;

			// Show the percentage on our label.
			labelPerc.Text = e.ProgressPercentage.ToString() + "%";

			// Update the label with how much data have been downloaded so far and the total size of the file we are currently downloading
			labelDownloaded.Text = string.Format("{0} MB / {1} MB",
				(e.BytesReceived / 1024d / 1024d).ToString("0.00"),
				(e.TotalBytesToReceive / 1024d / 1024d).ToString("0.00"));
		}

		// The event that will trigger when the WebClient is completed
		private void Completed(object sender, AsyncCompletedEventArgs e) {
			// Reset the stopwatch.
			Variables.sw.Reset();
			Status.Text = $"Pobrano {Variables.zipPath}";
			//unzip
			ZipStorer zip = ZipStorer.Open(Variables.zipPath, FileAccess.Read);
			List<ZipStorer.ZipFileEntry> dir = zip.ReadCentralDir();
			foreach (ZipStorer.ZipFileEntry entry in dir) {
				Variables.currentFile = Path.Combine(Variables.tempDir, entry.ToString());
				zip.ExtractFile(entry, Variables.currentFile);
			}
			//copy
			//dir
			Directory.CreateDirectory(Variables.klockiPath);
            foreach (string dirPath in Directory.GetDirectories(Variables.extractedPath, "*",
                SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(Variables.extractedPath, Variables.klockiPath));

            //copy files
            foreach (string newPath in Directory.GetFiles(Variables.extractedPath, "*.*",
                SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(Variables.extractedPath, Variables.klockiPath), true);

			DownloadFile(Variables.jsonUrlZyd, Variables.klockiJsonPath);

			Status.Text = $"Zainstalowano BlazingPack 1.8.8.";

		}

		private void MainWindow_Load(object sender, EventArgs e) {

		}

		private void PremiumButton_Click(object sender, EventArgs e) {
			MessageBox.Show("Instalacja dla użytkowników Premium nie została jeszcze zaimplementowana. Zainstaluj BlazingPack ręcznie, lub spróbuj użyć instalatora non premium. Uwaga: Instalator non premium może usunąć dane logowania i profile z launchera Premium.");
		}
	}
}
