﻿namespace BlazedInstaller
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
			this.InstallButton = new System.Windows.Forms.Button();
			this.Status = new System.Windows.Forms.Label();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.labelSpeed = new System.Windows.Forms.Label();
			this.labelPerc = new System.Windows.Forms.Label();
			this.labelDownloaded = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.PremiumButton = new System.Windows.Forms.Button();
			this.Logo = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
			this.SuspendLayout();
			// 
			// InstallButton
			// 
			this.InstallButton.Location = new System.Drawing.Point(100, 200);
			this.InstallButton.Name = "InstallButton";
			this.InstallButton.Size = new System.Drawing.Size(300, 25);
			this.InstallButton.TabIndex = 0;
			this.InstallButton.Text = "ZAINSTALUJ BLAZINGPACK DLA NONPREMIUM";
			this.InstallButton.UseVisualStyleBackColor = true;
			this.InstallButton.Click += new System.EventHandler(this.InstallButton_Click);
			// 
			// Status
			// 
			this.Status.AutoSize = true;
			this.Status.Location = new System.Drawing.Point(120, 251);
			this.Status.Name = "Status";
			this.Status.Size = new System.Drawing.Size(260, 13);
			this.Status.TabIndex = 1;
			this.Status.Text = "BlazedInstaller v. 0.1  -  © Dawid Itrych (Nikuw) 2018.";
			this.Status.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// progressBar
			// 
			this.progressBar.Location = new System.Drawing.Point(25, 25);
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(450, 25);
			this.progressBar.TabIndex = 2;
			// 
			// labelSpeed
			// 
			this.labelSpeed.AutoSize = true;
			this.labelSpeed.Location = new System.Drawing.Point(430, 53);
			this.labelSpeed.Name = "labelSpeed";
			this.labelSpeed.Size = new System.Drawing.Size(39, 13);
			this.labelSpeed.TabIndex = 3;
			this.labelSpeed.Text = "0 kB/s";
			// 
			// labelPerc
			// 
			this.labelPerc.AutoSize = true;
			this.labelPerc.Location = new System.Drawing.Point(22, 53);
			this.labelPerc.Name = "labelPerc";
			this.labelPerc.Size = new System.Drawing.Size(21, 13);
			this.labelPerc.TabIndex = 4;
			this.labelPerc.Text = "0%";
			// 
			// labelDownloaded
			// 
			this.labelDownloaded.AutoSize = true;
			this.labelDownloaded.Location = new System.Drawing.Point(220, 53);
			this.labelDownloaded.Name = "labelDownloaded";
			this.labelDownloaded.Size = new System.Drawing.Size(68, 13);
			this.labelDownloaded.TabIndex = 5;
			this.labelDownloaded.Text = "0 MB / 0 MB";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(30, 235);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(432, 13);
			this.label1.TabIndex = 6;
			this.label1.Text = "UWAGA! INSTALACJA ZŁEJ WERSJI MOŻE USZKODZIĆ INSTALACJĘ MINECRAFTA!";
			// 
			// PremiumButton
			// 
			this.PremiumButton.Location = new System.Drawing.Point(100, 169);
			this.PremiumButton.Name = "PremiumButton";
			this.PremiumButton.Size = new System.Drawing.Size(300, 25);
			this.PremiumButton.TabIndex = 7;
			this.PremiumButton.Text = "ZAINSTALUJ BLAZINGPACK DLA PREMIUM";
			this.PremiumButton.UseVisualStyleBackColor = true;
			this.PremiumButton.Click += new System.EventHandler(this.PremiumButton_Click);
			// 
			// Logo
			// 
			this.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.Logo.Image = global::BlazedInstaller.Properties.Resources.logo;
			this.Logo.Location = new System.Drawing.Point(25, 69);
			this.Logo.Name = "Logo";
			this.Logo.Size = new System.Drawing.Size(450, 94);
			this.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.Logo.TabIndex = 8;
			this.Logo.TabStop = false;
			// 
			// MainWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(494, 275);
			this.Controls.Add(this.Logo);
			this.Controls.Add(this.PremiumButton);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.labelDownloaded);
			this.Controls.Add(this.labelPerc);
			this.Controls.Add(this.labelSpeed);
			this.Controls.Add(this.progressBar);
			this.Controls.Add(this.Status);
			this.Controls.Add(this.InstallButton);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "MainWindow";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "BlazedInstaller";
			this.TopMost = true;
			this.Load += new System.EventHandler(this.MainWindow_Load);
			((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button InstallButton;
		private System.Windows.Forms.Label Status;
		private System.Windows.Forms.ProgressBar progressBar;
		private System.Windows.Forms.Label labelSpeed;
		private System.Windows.Forms.Label labelPerc;
		private System.Windows.Forms.Label labelDownloaded;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button PremiumButton;
		private System.Windows.Forms.PictureBox Logo;
	}
}

